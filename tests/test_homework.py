import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage
from pages.projects_page import ProjectsPage
import string
import random


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    login_page.load()
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")
    yield browser
    browser.quit()


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


def test_logout_correctly_displeyed(browser):
    assert browser.find_element(By.CSS_SELECTOR, "[title=Wyloguj]").is_displayed() is True


def test_open_administration(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()

    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'


def test_add_and_find_new_project(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    browser.find_element(By.CSS_SELECTOR, '.button_link').click()
    random = get_random_string(8)
    name = random
    project_name = browser.find_element(By.CSS_SELECTOR, '[name="name"]').send_keys(name)
    project_prefix = browser.find_element(By.CSS_SELECTOR, '[name="prefix"]').send_keys(random)
    project_save = browser.find_element(By.CSS_SELECTOR, '[name="save"]').click()
    browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()
    browser.find_element(By.CSS_SELECTOR, '[name="search"]').send_keys(name)
    browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

def test_open_projects(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    projects_page = ProjectsPage(browser)
    projects_page.click_projects()




