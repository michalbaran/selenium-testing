from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

class ProjectsPage:
    def __init__(self, browser):
        self.browser = browser

    def click_administration(self):
        admin_button = self.browser.find_element(By.CSS_SELECTOR, '[title=Administracja]')
        admin_button.click()

    def click_projects(self):
        projects = self.browser.find_element(By.CSS_SELECTOR, '.activeMenu')
        projects.click()